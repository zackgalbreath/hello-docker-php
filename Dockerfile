FROM ubuntu:jammy

ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=Etc/UTC

RUN apt-get update && \
    apt-get install -y \
        libapache2-mod-php && \
    apt-get clean

COPY phpinfo.php /var/www/html/

ENTRYPOINT ["/usr/sbin/apache2ctl", "-D","FOREGROUND"]
